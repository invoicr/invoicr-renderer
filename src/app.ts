import express = require('express');
import puppeteer = require('puppeteer');
import winston = require('winston');
const { format } = require('logform');

const alignedWithColorsAndTime = format.combine(
  format.colorize(),
  format.timestamp(),
  format.align(),
  format.printf((info: any) => `${info.timestamp} ${info.level}: ${info.message}`)
);

const logger = winston.createLogger({
  level: 'info',
});

logger.add(new winston.transports.Console({
  format: alignedWithColorsAndTime
}));

// Create a new express application instance
const app: express.Application = express();
const browserPrestarted = puppeteer.launch({
  args: ['--disable-dev-shm-usage', '--no-sandbox']
});

app.get('/', function (req: any, res: any) {
  res.send('Welcome to the invoice renderer!');
});

app.get('/render/:filename', async (req: any, res: any, next: any) => {
  let page;

  try {
    const browser = await browserPrestarted;
    page = await browser.newPage();
    let url = new URL('https://invoicr.app');
    for (let key in req.query) {
      url.searchParams.set(key, req.query[key]);
    }
    await page.goto(url.toString());
    const pdf = await page.pdf({
      printBackground: true,
      format: 'A4',
      margin: {
        top: 0,
        right: 0,
        bottom: 0,
        left: 0
      }
    });
    res.writeHead(200, { 'content-type': 'application/pdf' });
    res.write(pdf, 'binary');
    res.end(null, 'binary');
    logger.info("Successfully rendered " + req.params['filename']);
  } catch (e) {
    logger.error(e);
    next(e);
  } finally {
    if (page) {
      page.close();
    }
  }
});

app.listen(5000, function () {
  logger.info('Listening on port 5000!');
});

process.on('SIGTERM', function () {
  browserPrestarted
    .then((b: any) => { return b.close(); })
    .finally(() => process.exit(0));
});
